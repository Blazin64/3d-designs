//!OpenSCAD
// Number of motors
motors = 1; // [1:1:25]
// Distance between motor mounts
gap = 25; // [0:No Gap, 25:25mm Gap]
// Size of screw holes in mm
hole_size = 3.5; // [3:0.1:5]

build();

module build(){
    base();
    translate([15,0,0])for(x=[0:motors-1]){
        difference(){
            translate([(gap+47)*x,0,0])frame();
            translate([(gap+47)*x,0,0])holes();
        }
    }
}
module base(){
    difference(){
        cube([30+47*motors+3+gap*(motors-1),20,5]);
        translate([7.5,10,0])cylinder($fn=32,r=hole_size/2,h=7);
        translate([30+47*motors-4.5+gap*(motors-1),10,0])cylinder($fn=32,r=hole_size/2,h=7);
        if(gap>=25 && motors>1){
            for(x=[1:motors-1]){
                translate([-8.5+(gap/2)+(gap+47)*x,10,0])cylinder($fn=32,r=hole_size/2,h=7);
            }
        }
    }
}
module frame(){
    difference(){
        translate([0,0,5])cube([50,20,47]);
        translate([3,2,5])cube([44,20,48]);
        translate([-0.1,0.85 ,55])rotate([-69.04,0,0])cube(100);
    }
}
module holes(){
    translate([25,3,28.5])rotate([90,0,0])cylinder($fn=128, r=12, h=7);
    translate([9.5,3,44])rotate([90,0,0])cylinder($fn=32, r=hole_size/2, h=7);
    translate([9.5,3,13])rotate([90,0,0])cylinder($fn=32, r=hole_size/2, h=7);
    translate([40.5,3,13])rotate([90,0,0])cylinder($fn=32, r=hole_size/2, h=7);
    translate([40.5,3,44])rotate([90,0,0])cylinder($fn=32, r=hole_size/2, h=7);
}
