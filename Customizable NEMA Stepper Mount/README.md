License: CC BY-NC-SA 4.0

**Description:**
This is a ridiculously customizable NEMA 17 stepper motor mount that can support as many motors as you want. Use T-nuts to attach it to V-slot extrusions.

The Thingiverse Customizer is limited to 25 motors, but you can go higher if you download and render the OpenSCAD file on your own.

**Images:**

![](images/photo-render.jpeg)

**Features:**

* Supports an infinite number of motors. Now we just need an infinite 3D printer...
* Adjustable screw hole size. (Defaults to 3.5mm)
* Optional gap and screw hole between mounts, for better stability.

**Updates:**

2020-03-11: Increased screw hole size to 3.5mm. Centered all screw holes. Moved base holes 2.5mm closer to motor frame on X axis. Moved NEMA holes 1.5mm higher on Z axis.
