# 3D Designs

A collection of printable 3D designs that I have created or modified. These designs can also be found on Thingiverse.

# Original Designs

| Name                            | Links                                                                                                | License                                                               |
|---------------------------------|------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------|
| Customizable NEMA Stepper Mount | [GitLab](Customizable NEMA Stepper Mount/), [Thingiverse](https://www.thingiverse.com/thing:4210660) | [CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/) |
| MicroKORG XL Mic Repair Clamp   | [GitLab](MicroKORG XL Mic Repair Clamp/), [Thingiverse](https://www.thingiverse.com/thing:4206527)    | [CC BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/)       |

# Modified Designs

Because these designs are derivatives, they fall under the original authors' licenses.

| Name                                           | Links                                                                                                               | License                                                               |
|------------------------------------------------|---------------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------|
| Treefrog Dual Extruder Conversion Tool         | [GitLab](Treefrog Dual Extruder Conversion/), [Thingiverse](https://www.thingiverse.com/thing:4204604)              | [CC BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/)       |
| 2020 V-slot mount for 18mm U-channel LED Strip | [GitLab](2020 V-slot mount for 18mm U-channel LED Strip/), [Thingiverse](https://www.thingiverse.com/thing:4210649) | [CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/) |
| Ender 3 Y-Axis Modular Mounting System Adapter | [GitLab](Ender 3 Y-Axis Modular Mounting System Adapter/), [Thingiverse](https://www.thingiverse.com/thing:4206396) | [CC BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/)       |
| US Quarter Dispenser W/ Belt Clip              | [GitLab](US Quarter Dispenser W Belt Clip/), [Thingiverse](https://www.thingiverse.com/thing:4208720)               | [CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/) |

