License: CC BY-SA 3.0

**Description:**

What I provided here is an OpenSCAD script that will cut the model into separate pieces. You will need to download [MorenaP’s Treefrog model](https://www.thingiverse.com/thing:18479) separately.

While the script is only designed to split the model for dual-extruder printing, it can be easily modified to split the model into even more sections for printing with more than two extruders.

**Images**

![](images/photo-printed.jpeg)
![](images/photo-render-body.jpeg)
![](images/photo-render-eyes-feet.jpeg)

**Model License:**

Please bear in mind that the resulting 3D models and prints will be under MorenaP's license terms (see the [model's page](https://www.thingiverse.com/thing:18479)). Basically, they are for private use only. Don't use them commercially or post them anywhere.

MorenaP is fine with people posting pictures of makes on Thingiverse. If you intend to post pictures elsewhere (like social media), be sure to ask permission.

**OpenSCAD Script License:**

You are free to modify and share my script as you see fit. In fact, I encourage it. While my script is reasonably good, there is always room for improvement. Just be sure to remember the model license details mentioned above. ;)

**How to use:**

01. Install [OpenSCAD](https://www.openscad.org/), if you haven’t already.

02. Download [MorenaP’s Treefrog model](https://www.thingiverse.com/thing:18479) and my OpenSCAD script.

03. Put the model STL (treefrog_45_cut.stl) and OpenSCAD script (treefrog-dual-conversion.scad) in the same folder.

04. Load the script in OpenSCAD.

05. Press F6 or click Design > Render. This may a long time, depending on your computer.

06. Save the result as an STL. Press F7 or click File > Export > Export as STL. Use whatever file name you like, e.g. “treefrog_body.stl.”

07. Change “part = 0;” to “part = 1;” at the top line in the OpenSCAD editor.

08. Press F6 or click Design > Render. This may a long time, depending on your computer.

09. Save the result as an STL. Press F7 or click File > Export > Export as STL. Use whatever file name you like, e.g. “treefrog_eyes_feet.stl.”

10. Import the two new STL files into your slicer. You know what to do now, right?
