//Set to 0 for body, 1 for eyes & feet
part = 0;

if (part == 0){
    body_only();
}
if (part == 1){
    eyes_feet_only();
}
module eyes(){
    //left
    translate([-8.8,-12,20.9])rotate([65,-7,-33])scale([1,0.87,1])cylinder(r=4.16,h=5,$fn=32);
    //right
    translate([8.8,-12,20.9])rotate([65,7,33])scale([1,0.87,1])cylinder(r=4.16,h=5,$fn=32);
}
module feet(){
    //back feet
    translate([11.5,-6.5,0])rotate([0,0,25])cube([14,6.5,2.2]);
    translate([18,-9,0])rotate([0,0,25])cube([6,6,2.2]);
    translate([-24,-0.5,0])rotate([0,0,-25])cube([14,6.5,2.2]);
    translate([-23.5,-6.5,0])rotate([0,0,-25])cube([6,6,2.2]);
    //front feet
    translate([14.5,-19.8,-7])rotate([45,0,45])cube([13.5,10,10]);
    translate([-24.1,-10.3,-7])rotate([45,0,-45])cube([13.5,10,10]);
}
module body_only(){
    difference(){
        import("treefrog_45_cut.stl");
        feet();
        eyes();
    }
}
module eyes_feet_only(){
    difference(){
        import("treefrog_45_cut.stl");
        difference(){
            translate([-50,-50,0])cube(100);
            eyes();
            feet();
        }
    }
}