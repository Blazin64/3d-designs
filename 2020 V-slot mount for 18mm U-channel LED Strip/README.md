License: CC BY-NC-SA 4.0

**Description:**

I liked [RogueGeek's design](https://www.thingiverse.com/thing:3361566) and chose to re-purpose it for my 18mm U-channel LED strips. Along with nearly any other printer with 2020 V-slots, it works with the Ender 3 and Ender 3 Pro.

**Images:**

![](images/photo-printed.jpeg)
![](images/photo-render.jpeg)
