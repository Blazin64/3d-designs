License: CC BY-SA 3.0

**Description:**
My sister's MicroKorg XL microphone broke just at the top of the gooseneck. I made this clamp as a quick repair. It requires a lot of tension, so I printed it in PETG. Stiffer filaments like PLA may break.

I designed it in OpenSCAD, so you can easily modify it to fit your needs.

**Images:**

![](images/photo-printed-installed.jpeg)
![](images/photo-printed-closeup.jpeg)

**How to Use:**

Print two copies of the clamp and assemble it with four M3 bolts and nuts. It needs to be tightened as much as possible without breaking.
