difference(){
    clamp_body();
    mic_neck();
    screw_holes();
}

module mic_neck(){
    cylinder($fn=64, r=5.45, h=8.2);
    translate([0,0,8.2])cylinder($fn=64, r=4.125, h=11.8);
}
module screw_holes(){
    translate([-10,-7.7,3])rotate([0,90,0])cylinder($fn=32, r=1.6, h=20);
    translate([-10,7.7,3])rotate([0,90,0])cylinder($fn=32, r=1.6, h=20);
    translate([-10,7.7,17])rotate([0,90,0])cylinder($fn=32, r=1.6, h=20);
    translate([-10,-7.7,17])rotate([0,90,0])cylinder($fn=32, r=1.6, h=20);
}
module clamp_body(){
    difference(){
        translate([-7,-10,0])cube([6.5,20,20]);
    }
}