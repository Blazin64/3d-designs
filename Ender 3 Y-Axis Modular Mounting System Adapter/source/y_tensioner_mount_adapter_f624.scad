import("SRC_Tensioner_-_Screw_-_Ver._F624_+_M4_-_[_Mk7_Flipped_and_XYZ_0.STL");
translate([2.5,-30,0])mount_attachment();


module mount_attachment(){
    union(){
        difference(){
            import("SRC_Arm_Axis_Changer_v5_Flipped_and_XYZ_0.stl");
            translate([-10,18.8,0])cube([50,20,16]);
        }
        translate([2,18.8,0])cube([15,0.2,15]);
        translate([3,18.8,0])cube([13,20,15]);
    }
}
