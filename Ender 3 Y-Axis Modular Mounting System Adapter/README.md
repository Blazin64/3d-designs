License: CC BY-SA 3.0

**Description:**

This is an adapter for using yyh1002's [Modular Mounting System](https://www.thingiverse.com/thing:2194278) on Cornely_Cool's [Ender 3](https://www.thingiverse.com/thing:3097972) and [Ender 3 Pro](https://www.thingiverse.com/thing:3289057) Y axis tensioners.

**Images:**

![](images/photo-printed.jpeg)
![](images/photo-render.jpeg)

**Bearing Compatibility:**

This adapter is meant to be used with the stock F624 bearings.

**What Parts To Download & Print:**

* From this thing "y_tensioner_mount_adapter_f624.stl"
* [Ender 3 Mod - Y Axis Tensioner](https://www.thingiverse.com/thing:3097972): "[_Ender_3__-_Y_Tensioner_-_Body_OEM_-_[_Mk7_.stl" if you have an Ender 3.
* [Ender 3 Pro Mod - Y Axis Tensioner](https://www.thingiverse.com/thing:3289057): "[_Ender_3_Pro__-_Y_Tensioner_-_Body_OEM_-_T-Nuts_-_[_Mk7_.stl" if you have an Ender 3 Pro.
* [Y Tensioner mod for Camera Mount System](https://www.thingiverse.com/thing:3406470): "Mk2__-_Ender_3_Pro_-_OEM_-_Y_Tensioner_-_Knob_shortened.stl"
* [Modular Mounting System](https://www.thingiverse.com/thing:2194278): Your choice of mounting arms and accessories.
* [Logitech C920 holder](https://www.thingiverse.com/thing:4142537): If you plan to mount a Logitech C920.

Ignore the files with "SRC" in the name, unless you intend to use the OpenSCAD script.
