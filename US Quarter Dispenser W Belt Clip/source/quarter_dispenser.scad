//!OpenSCAD
//
// Remix of https://www.thingiverse.com/thing:3068593
//
// To center the model at XYZ 0, I loaded it in Prusa
// Slicer and centered it before exporting it as an STL.
// It appears to be a lossless process. The file size
// remained the same.
//
// The centered model was simplified in Meshlab, to allow
// successful modifications in OpenSCAD. The original
// dispenser was simplified with Quadratic Edge Collapse
// Decimation, using defaults with "Planar Simplification" 
// checked and "Target number of faces" set to 4,996.
// There was no visible loss in detail
//
union(){
    holder_cut();
    belt_clip();    
}
module remove_text(){
    //remove the "wash coins" text from the side
    difference(){
        union(){
            import("SRC_Long_Dispenser_XYZ_0_aligned_simplified.stl");
            translate([-5,-15.361,25])cube([10,1,75]);
        }
        translate([-25,-16.361,0])cube([50,1,150]);
    }
}
module holder_cut(){
    //cut the holder to suit US quarters.
    difference(){
    rotate([0,0,-30])remove_text();
    translate([-4,-12.65,3.17])cube([24,25.3,2.5]);//quarter size slot
    translate([-25.25,-8.29,0])cube([50.5,16.58,5.65]);//full-length opening
    translate([0,0,3.3])cylinder(r=13.75,h=101.8);//larger inner diameter for bigger springs
}
}
module belt_clip(){
    translate([-17,-8,14])rotate([90,0,180])cylinder($fn=3,h=16,r=6); //wedge
    translate([-23,-8,15])cube([9,16,5]); //fill top of wedge
    translate([-23,-8,14])cube([3,16,53]); //clip side
    translate([-23,-8,64])cube([4.5,16,3]); //clip lip
}
