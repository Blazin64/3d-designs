License: CC BY-NC-SA 4.0

**Description:**

I regularly visit arcades and needed a quarter dispenser that can be clipped onto my belt. [Skiid's coin dispenser](https://www.thingiverse.com/thing:3068593) looked like a good basis, so this remix was born.

**Images:**

![](images/photo-printed.jpeg)
![](images/photo-printed-cap.jpeg)

**Features/Improvements:**

* There is a belt clip on the side, so you don't have to carry it in your hands or dig it out of your pocket.

* The coin slot is taller to better suit US quarters. The original design required a lot of sanding.

* Larger, more common, springs with a 3/4 inch or 19 mm diameter (possibly a little bigger) are supported.

* The included spring spacer is tall enough to avoid jamming. Now there is no need to glue two shorter ones together!

* The thumb opening at the top has been extended to the width of the dispenser. This allows you to more easily remove quarters.
